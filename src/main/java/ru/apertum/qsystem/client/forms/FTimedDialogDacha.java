/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.client.forms;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import ru.apertum.qsystem.client.QProperties;
import ru.apertum.qsystem.client.common.WelcomeParams;
import ru.apertum.qsystem.client.model.QButton;
import ru.apertum.qsystem.client.model.QPanel;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.common.Uses;
import ru.apertum.qsystem.common.model.ATalkingClock;

import java.awt.Font;
import java.awt.Frame;
import ru.apertum.qsystem.plugin.Owners;


/**
 * Created on 15.01.2010, 17:49:14.
 *
 * @author Evgeniy Egorov
 */
@SuppressWarnings({"squid:S1192", "squid:S1172", "squid:S1450", "squid:S1604", "squid:S1161", "squid:MaximumInheritanceDepth"})
public class FTimedDialogDacha extends javax.swing.JDialog {

    private static FTimedDialogDacha dialog;

    /**
     * Creates new form FTimedDialog.
     *
     * @param parent
     * @param modal
     */
    public FTimedDialogDacha(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        buttonClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ru/apertum/qsystem/client/forms/resources/stop.png"))); // NOI18N
        if (WelcomeParams.getInstance().btnFont != null) {
            buttonClose.setFont(WelcomeParams.getInstance().btnFont);
        } else {
            buttonClose.setFont(Font.decode("Tahoma-Plain-36"));
        }
    }

    private Owners.Owner result;

    /**
     * Статический метод который показывает модально диалог регистрации клиентов.
     *
     * @param parent     фрейм относительно которого будет модальность
     * @param modal      модальный диалог или нет
     * @param fullscreen
     * @param message    сообщение для показа.
     * @param timeout    сколько мсек. показываем.
     * @param owner
     * @return может быть NULL.
     */
    public static Owners.Owner showTimedDialog(Frame parent, boolean modal, boolean fullscreen, String message, int timeout, Owners.Owner owner) {
        if (dialog == null) {
            dialog = new FTimedDialogDacha(parent, modal);
            dialog.setTitle("Сообщение.");
        }
        dialog.result = owner;
        dialog.setBounds(10, 10, 1024, 400);
        dialog.changeTextToLocale();
        dialog.labelMess.setText(message);
        final String confirmCaption1 = QProperties.get().getProperty(Owners.SECTION, "confirm_caption1", "<html><span style='font-size:40.0pt;color:green'>DACHA.confirm_caption1<span>");
        final String confirmCaption2 = QProperties.get().getProperty(Owners.SECTION, "confirm_caption2", "<html><span style='font-size:40.0pt;color:yellow'>DACHA.confirm_caption2<span>");
        final String confirmCaption3 = QProperties.get().getProperty(Owners.SECTION, "confirm_caption3", "<html><span style='font-size:40.0pt;color:red'>DACHA.confirm_caption3<span>");
        dialog.jButton1.setText(confirmCaption1);
        dialog.jButton2.setText(confirmCaption2);
        dialog.jButton3.setText(confirmCaption3);

        dialog.setSize(FWelcome.getScreenHorizontal(), FWelcome.getScreenVertical());
        Uses.setLocation(dialog);
        dialog.changeTextToLocale();
        if (!(QConfig.cfg().isDebug() || QConfig.cfg().isDemo() && !fullscreen)) {
            Uses.setFullSize(dialog);
            if (QConfig.cfg().isHideCursor()) {
                int[] pixels = new int[16 * 16];
                Image image = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(16, 16, pixels, 0, 16));
                Cursor transparentCursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(0, 0), "invisibleCursor");
                dialog.setCursor(transparentCursor);
            }
        } else {
            dialog.setSize(FWelcome.getScreenHorizontal(), FWelcome.getScreenVertical());
            Uses.setLocation(dialog, parent);
        }

        dialog.clockClose = new ATalkingClock(timeout, 1) {

            @Override
            public void run() {
                dialog.buttonCloseActionPerformed(null);
            }
        };
        dialog.clockClose.start();
        dialog.setVisible(true);
        return dialog.result;
    }

    /**
     * Таймер висения диалога на мониторе.
     */
    private transient ATalkingClock clockClose;

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new QPanel(WelcomeParams.getInstance().getBackgroundImg("input"));
        jPanel2 = new javax.swing.JPanel();
        buttonClose = new QButton(WelcomeParams.getInstance().servButtonType);
        labelMess = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setName("Form"); // NOI18N
        setUndecorated(true);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(FTimedDialogDacha.class);
        jPanel1.setBackground(resourceMap.getColor("jPanel1.background")); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setOpaque(false);

        jPanel2.setBackground(resourceMap.getColor("jPanel2.background")); // NOI18N
        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setOpaque(false);

        buttonClose.setFont(resourceMap.getFont("buttonClose.font")); // NOI18N
        buttonClose.setIcon(resourceMap.getIcon("buttonClose.icon")); // NOI18N
        buttonClose.setText(resourceMap.getString("buttonClose.text")); // NOI18N
        buttonClose.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, resourceMap.getColor("buttonClose.border.outsideBorder.highlightInnerColor"), null, null), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED))); // NOI18N
        buttonClose.setName("buttonClose"); // NOI18N
        buttonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCloseActionPerformed(evt);
            }
        });

        labelMess.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelMess.setText(resourceMap.getString("labelMess.text")); // NOI18N
        labelMess.setName("labelMess"); // NOI18N

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridLayout(3, 1, 50, 80));

        jButton1.setText(resourceMap.getString("jButton1.text")); // NOI18N
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);

        jButton2.setText(resourceMap.getString("jButton2.text")); // NOI18N
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton2);

        jButton3.setText(resourceMap.getString("jButton3.text")); // NOI18N
        jButton3.setName("jButton3"); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelMess, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(buttonClose, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(70, 70, 70))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelMess, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(buttonClose, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void changeTextToLocale() {
        final org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(ru.apertum.qsystem.QSystem.class).getContext().getResourceMap(FTimedDialogDacha.class);
        buttonClose.setText(resourceMap.getString("buttonClose.text")); // NOI18N
    }

    private void buttonOkActionPerformed() {
        if (clockClose.isActive()) {
            clockClose.stop();
        }
        clockClose = null;
        setVisible(false);
    }

    private void buttonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCloseActionPerformed
        if (clockClose.isActive()) {
            clockClose.stop();
        }
        clockClose = null;
        result.detail = null;
        setVisible(false);
    }//GEN-LAST:event_buttonCloseActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        result.detail = "1";
        buttonOkActionPerformed();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        result.detail = "2";
        buttonOkActionPerformed();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        result.detail = "3";
        buttonOkActionPerformed();
    }//GEN-LAST:event_jButton3ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonClose;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private ru.apertum.qsystem.client.model.QPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel labelMess;
    // End of variables declaration//GEN-END:variables
}
