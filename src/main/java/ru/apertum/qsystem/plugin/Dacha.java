package ru.apertum.qsystem.plugin;

import ru.apertum.qsystem.client.QProperties;
import ru.apertum.qsystem.client.common.WelcomeParams;
import ru.apertum.qsystem.client.forms.FInputDachaDialog;
import ru.apertum.qsystem.client.forms.FWelcome;
import ru.apertum.qsystem.client.model.QButton;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.cmd.RpcGetAllServices;
import ru.apertum.qsystem.common.model.IClientNetProperty;
import ru.apertum.qsystem.common.model.INetProperty;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.IWelcome;
import ru.apertum.qsystem.server.model.QAdvanceCustomer;
import ru.apertum.qsystem.server.model.QProperty;
import ru.apertum.qsystem.server.model.QService;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;

public class Dacha implements IWelcome {

    private static Owners.Owner owner = null;

    /**
     * Перебор всех услуг до одной включая корень и узлы.
     */
    private QService sailToStorm(QService root, Long id) {
        return root.getId().equals(id) ? root : seil(root, id);
    }

    private QService seil(QService parent, Long id) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            if (parent.getChildAt(i).getId().equals(id)) {
                return parent.getChildAt(i);
            } else {
                final QService seil = seil(parent.getChildAt(i), id);
                if (seil != null) {
                    return seil;
                }
            }
        }
        return null;
    }

    @Override
    public void start(IClientNetProperty iClientNetProperty, RpcGetAllServices.ServicesForWelcome servicesForWelcome) {
        QLog.l().logger().info("DACHA. Start");
        QLog.l().logger().info("DACHA.group_only = {}", QProperties.get().getProperty(Owners.SECTION, "group_only", "1"));
        QLog.l().logger().info("DACHA.root_only = {}", QProperties.get().getProperty(Owners.SECTION, "root_only", "1"));
        QLog.l().logger().info("DACHA.restrict_absent = {}", QProperties.get().getProperty(Owners.SECTION, "restrict_absent", "1"));
        QLog.l().logger().info("DACHA.caption = {}", QProperties.get().getProperty(Owners.SECTION, "caption", "Empty"));
        QLog.l().logger().info("DACHA.caption1 = {}", QProperties.get().getProperty(Owners.SECTION, "caption1", "Номер участка"));
        QLog.l().logger().info("DACHA.caption2 = {}", QProperties.get().getProperty(Owners.SECTION, "caption2", "Номер очереди"));
        QLog.l().logger().info("DACHA.html_info = {}", QProperties.get().getProperty(Owners.SECTION, "html_info", "#text"));
        QLog.l().logger().info("DACHA.html_error = {}", QProperties.get().getProperty(Owners.SECTION, "html_error", "#text"));
        QLog.l().logger().info("DACHA.charset = {}", QProperties.get().getProperty(Owners.SECTION, "charset", "cp1251"));
        QLog.l().logger().info("DACHA.color1 = {}", Color.decode(QProperties.get().getProperty(Owners.SECTION, "color1", "#FFFFFF")));
        QLog.l().logger().info("DACHA.font1 = {}", Font.decode(QProperties.get().getProperty(Owners.SECTION, "font1", "Tahoma-Plain-36")));
        QLog.l().logger().info("DACHA.file = {}", QProperties.get().getProperty(Owners.SECTION, Owners.FILE));
        for (int i = 0; i < 100; i++) {
            final QProperty prop = QProperties.get().getProperty(Owners.SECTION, Integer.toString(i));
            if (prop != null) {
                QLog.l().logger().info("DACHA. service mapping: column={}; id={}; service={}", i, prop.getValue(),
                        prop.getValueAsLong(-1) == -1 ? "NO" : sailToStorm(servicesForWelcome.getRoot(), prop.getValueAsLong()));
            }
        }
        QLog.l().logger().info("DACHA. go...");
    }

    @Override
    public boolean showPreInfoDialog(QButton qButton, Frame frame, INetProperty iNetProperty, String s, String s1, boolean b, boolean b1, int i) {
        return false;
    }

    @Override
    public String showInputDialog(QButton qButton, Frame frame, boolean b, INetProperty iNetProperty, boolean b1, int i, String s, QService qService) {
        // типа ввели эту строку.
        return owner == null ? null : (owner.field + " - " + owner.region + " / " + owner.detail + ":" + owner.name);
    }

    @Override
    public StandInParameters handleStandInParams(QButton qButton, StandInParameters params) {
        // ничего не меняем, оставляем как есть.
        return params;
    }

    @Override
    public boolean buttonPressed(QButton qButton, QService service) {
        //`DACHA.group_only`{=0/1} - Запрашивать ввод только для групп услуг. По умолчанию(=1) диалог только по нажатию на группы услуг.
        final boolean groupOnly = "1".equals(QProperties.get().getProperty(Owners.SECTION, "group_only", "1"));

        //`DACHA.root_only`{=0/1} - Применять плагин только для первого уровня. По умолчанию(=1) достаточно провалиться на второй уровень по группам и это считается достаточным для идентификации.
        final boolean rootOnly = "1".equals(QProperties.get().getProperty(Owners.SECTION, "root_only", "1"));


        if (qButton.isIsActive() && (!groupOnly || !service.isLeaf())) {
            QLog.l().logger().info("DACHA: input for service = {}; for groups = {}; only for root = {}", service, groupOnly, rootOnly);
            try {
                if (owner == null || !rootOnly || service.getParent().isRoot()) {
                    owner = FInputDachaDialog.showInputDialog(qButton.getForm(), true, FWelcome.getNetProperty(), false,
                            WelcomeParams.getInstance().delayBack, service.getTextToLocale(QService.Field.INPUT_CAPTION));
                    QLog.l().logger().trace("DACHA: owner = {}", owner);
                    if (owner == null) {
                        QLog.l().logger().trace("DACHA: false. ignored service {}", service);
                        return false;
                    }
                }
                // если для услуги явно не указано ограничение, то это воспринимать как запрет или как разрешение в зависимости от настройки "restrict_absent",
                // по умолчанию жестко указывать для разрешения.
                final String restrict = QProperties.get().getProperty(Owners.SECTION, "restrict_absent", "1");
                QLog.l().logger().info("DACHA: {}; restrict_absent={}", service, restrict);
                service.getChildren().forEach(ser ->
                        ser.setStatus(("0".equals(restrict) && owner.services.get(ser.getId()) == null) ||
                                (owner.services.get(ser.getId()) != null && owner.services.get(ser.getId()))
                                ? 1 : 0)
                );
            } catch (Exception ex) {
                QLog.l().logger().error("DACHA: ERROR " + service, ex);
            }
        } else {
            QLog.l().logger().trace("DACHA: ignore. active={}; groupOnly={}; leaf={}", qButton.isIsActive(), groupOnly, service.isLeaf());
        }
        QLog.l().logger().trace("DACHA: true. Go to service {}", service);
        return true;
    }

    @Override
    public void readyNewCustomer(QButton qButton, QCustomer qCustomer, QService qService) {
        // empty
    }

    @Override
    public void readyNewAdvCustomer(QButton qButton, QAdvanceCustomer qAdvanceCustomer, QService qService) {
        // empty
    }

    @Override
    public String getDescription() {
        return "Dacha SPB!";
    }

    @Override
    public long getUID() {
        return 29374530L;
    }
}
