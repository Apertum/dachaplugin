package ru.apertum.qsystem.plugin;

import ru.apertum.qsystem.client.QProperties;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.server.model.QProperty;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class Owners {
    public static final String SECTION = "DACHA";
    public static final String FILE = "file";

    public static class Uchastok implements Comparable {
        final String field;
        final String region;

        public Uchastok(String field, String region) {
            this.field = field;
            this.region = region;
        }

        @Override
        public int compareTo(Object o) {
            return o instanceof Uchastok && equals(o) ? 0 : (o instanceof Uchastok && field.equals(((Uchastok) o).field) ? 1 : -1);
        }

        @Override
        public int hashCode() {
            return (field + region).hashCode();
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof Uchastok && field.equals(((Uchastok) o).field) && region.equals(((Uchastok) o).region);
        }

        @Override
        public String toString() {
            return field + "-" + region;
        }
    }

    public static class Owner {
        final String field;
        final String region;
        public final String name;
        public final Map<Long, Boolean> services;
        public String detail;
        public final String error;

        public Owner(String error) {
            this.error = error;
            name = null;
            services = null;
            this.field = null;
            this.region = null;
        }

        public Owner(String name, Map<Long, Boolean> services, String field, String region) {
            this.name = name;
            this.services = services;
            error = null;
            this.field = field;
            this.region = region;
        }

        @Override
        public String toString() {
            return error == null ? name + " " + services : ("(" + field + " - " + region + ") " + error);
        }
    }

    private static Owner parseLine(String line) {
        final String[] strings = line.trim().split("\\s*;\\s*");
        if (strings.length > 3) {
            Map<Long, Boolean> map = new HashMap<>();
            for (int i = 1; i <= strings.length; i++) {
                if (PERMISSIONS.get(i) != null) {
                    map.put(PERMISSIONS.get(i), strings[i - 1].equals("1"));
                }
            }
            return new Owner(strings[3], map, strings[1], strings[2]);
        } else {
            QLog.l().logger().info("DACHA: Bad string: (length={}}) {}", strings.length, line);
            return null;
        }
    }

    public static Map<Uchastok, Owner> loadFile(File file) {
        final HashMap<Uchastok, Owner> map = new HashMap<>();
        try {
            final String charset = QProperties.get().getProperty(SECTION, "charset", "cp1251");
            QLog.l().logger().info("DACHA: file '{}'; charset={}", file.getAbsolutePath(), charset);
            Files.readAllLines(file.toPath(), Charset.forName(charset)).forEach(line -> {
                final Owner owner = parseLine(line);
                if (owner != null) {
                    map.put(new Uchastok(owner.field, owner.region), owner);
                }
            });
            QLog.l().logger().info("DACHA: file read successfully, {} records.", map.size());
        } catch (IOException e) {
            QLog.l().logger().error("DACHA: Read file '" + file.getAbsolutePath() + "' error.", e);
        }
        return map;
    }

    private static final Map<Integer, Long> PERMISSIONS = new HashMap<>();

    public static Owner checkOwner(String field, String region) {
        PERMISSIONS.clear();
        // В настройках так же лежат связи колоок и услуг, т.е. в какой колонке в файлике за какую услугу стоят пермишены.
        for (int i = 0; i < 30; i++) {
            final QProperty servProp = QProperties.get().getProperty(SECTION, Integer.toString(i));
            if (servProp != null && servProp.getValueAsLong() != null) {
                PERMISSIONS.put(i, servProp.getValueAsLong());
            }
        }

        final QProperty fileProp = QProperties.get().getProperty(SECTION, FILE);
        if (fileProp == null || fileProp.getValue() == null) {
            return new Owner("Property '" + SECTION + "." + FILE + "' not found.");
        }
        final File file = new File(fileProp.getValue());
        if (file.exists()) {
            final Map<Uchastok, Owner> map = loadFile(file);
            final Owner owner = map.get(new Uchastok(field, region));
            return owner == null ? new Owner("Владелец не найден") : owner;
        } else {
            return new Owner("File '" + fileProp.getValue() + "' not found.");
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 30; i++) {
            PERMISSIONS.put(i, 0L + i);
        }
        final Map<Uchastok, Owner> map = loadFile(new File(args.length > 0 ? args[0] : "test.csv"));
        System.out.println("Loaded:");
        map.entrySet().forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
        System.out.println("--------------------------");
    }
}
